/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import business.system.EcoSystem;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.net.Socket;
import javax.swing.ImageIcon;

/**
 *
 * @author kkkevinxx
 */
public class Util implements Serializable{
    public static ImageIcon FillLabel(String path)
    {
        ImageIcon personPic = new ImageIcon(path);
        Image pictureL = personPic.getImage();
        Image upPicture = pictureL.getScaledInstance(129, 130, Image.SCALE_SMOOTH);
        ImageIcon pic = new ImageIcon(upPicture);
        return pic;
    }
    
    public static ImageIcon FillLabelLittle(String path)
    {
        ImageIcon personPic = new ImageIcon(path);
        Image pictureL = personPic.getImage();
        Image upPicture = pictureL.getScaledInstance(150, 100, Image.SCALE_SMOOTH);
        ImageIcon pic = new ImageIcon(upPicture);
        return pic;
    }
    
    public static ImageIcon FillLabelBig(String path)
    {
        ImageIcon personPic = new ImageIcon(path);
        Image pictureL = personPic.getImage();
        Image upPicture = pictureL.getScaledInstance(300, 200, Image.SCALE_SMOOTH);
        ImageIcon pic = new ImageIcon(upPicture);
        return pic;
    }
    
    public static void SendSystem(EcoSystem system) throws IOException, ClassNotFoundException{
        Socket client = new Socket("169.254.196.78", 4243);

       PrintWriter printWriter = new PrintWriter(client.getOutputStream());
       printWriter.println("1");
       printWriter.flush();
      

       //通过printWriter 来向服务器发送消息
       ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
       out.writeObject(system);
       out.flush();
      

       InputStreamReader inputStreamReader = new InputStreamReader(client.getInputStream());
       BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
       bufferedReader = new BufferedReader(inputStreamReader);
       String request1 = bufferedReader.readLine();
       System.out.println("己收到服务器消息：" + request1);

       printWriter.close();
       out.close();
       System.out.println("连接已建立.system已发送");
   }
}
